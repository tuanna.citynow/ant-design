import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Route} from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './customer-list.component.html',
  styles: []
})
export class CustomerListComponent implements OnInit{
  mapOfExpandData: { [key: string]: boolean } = {};

  listOfData1 = [
    {
      id: 1,
      username: 'thiendoan',
      fullName: 'Thiện Đoàn',
      address: '1 đường Tam Hà, phường Tam Phú, quận Thủ Đức, TPHCM',
      expand: false,
      status: 'ACTIVATED',
      lastLogin: new Date()
    },
    {
      id: 2,
      username: 'tuantuan',
      fullName: 'Anh Tuấn',
      address: '1 đường Tam Hà, phường Tam Phú, quận Thủ Đức, TPHCM',
      expand: false,
      status: 'NOT_ACTIVATED',
      lastLogin: new Date()
    },
    {
      id: 3,
      username: 'dithong',
      fullName: 'Di Thông',
      address: '1 đường Tam Hà, phường Tam Phú, quận Thủ Đức, TPHCM',
      expand: false,
      status: 'BANDED',
      lastLogin: new Date()
    },
    {
      id: 4,
      username: 'cocola',
      fullName: 'Cô K Cô La',
      address: '1 đường Tam Hà, phường Tam Phú, quận Thủ Đức, TPHCM',
      expand: false,
      status: 'ACTIVATED',
      lastLogin: new Date()
    },
    {
      id: 5,
      username: 'pesico',
      fullName: 'Pesico',
      address: '1 đường Tam Hà, phường Tam Phú, quận Thủ Đức, TPHCM',
      expand: false,
      status: 'BANDED',
      lastLogin: new Date()
    },
    {
      id: 6,
      username: 'vinamilk',
      fullName: 'Vina Milk',
      address: '1 đường Tam Hà, phường Tam Phú, quận Thủ Đức, TPHCM',
      expand: false,
      status: 'ACTIVATED',
      lastLogin: new Date()
    },
    {
      id: 7,
      fullName: 'Leanne Graham',
      username: 'Bret',
      address: '1 đường Tam Hà, phường Tam Phú, quận Thủ Đức, TPHCM',
      expand: false,
      status: 'ACTIVATED',
      lastLogin: new Date()
    },
    {
      id: 8,
      fullName: 'Clementine Bauch',
      username: 'Samantha',
      address: '1 đường Tam Hà, phường Tam Phú, quận Thủ Đức, TPHCM',
      expand: false,
      status: 'NOT_ACTIVATED',
      lastLogin: new Date()
    },
    {
      id: 9,
      fullName: 'Patricia Lebsack',
      username: 'Karianne',
      address: '1 đường Tam Hà, phường Tam Phú, quận Thủ Đức, TPHCM',
      expand: false,
      status: 'ACTIVATED',
      lastLogin: new Date()
    },
    {
      id: 10,
      fullName: 'Chelsey Dietrich',
      username: 'Kamren',
      address: '1 đường Tam Hà, phường Tam Phú, quận Thủ Đức, TPHCM',
      expand: false,
      status: 'ACTIVATED',
      lastLogin: new Date()
    },
    {
      id: 11,
      fullName: 'Mrs. Dennis Schulist',
      username: 'Leopoldo_Corkery',
      address: '1 đường Tam Hà, phường Tam Phú, quận Thủ Đức, TPHCM',
      expand: false,
      status: 'ACTIVATED',
      lastLogin: new Date()
    },
  ];

  listOfData2 = [
    {
      id: 1,
      username: 'nicholasV',
      fullName: 'Nicholas Runolfsdottir V',
      address: '1 đường Tam Hà, phường Tam Phú, quận Thủ Đức, TPHCM',
      expand: false,
      status: 'ACTIVATED',
      lastLogin: new Date()
    },
    {
      id: 2,
      username: 'JoSmi',
      fullName: 'John Smith',
      address: '1 đường Tam Hà, phường Tam Phú, quận Thủ Đức, TPHCM',
      expand: false,
      status: 'NOT_ACTIVATED',
      lastLogin: new Date()
    },
    {
      id: 3,
      username: 'howell',
      fullName: 'Ervin Howell',
      address: '1 đường Tam Hà, phường Tam Phú, quận Thủ Đức, TPHCM',
      expand: false,
      status: 'ACTIVATED',
      lastLogin: new Date()
    }
  ];

  constructor(){}

  ngOnInit(): void {
  }
}
