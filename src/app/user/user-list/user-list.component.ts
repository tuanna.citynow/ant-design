import {Component, OnInit} from '@angular/core';
import {UserService} from '../../api/service/user.service';
import {Page} from '../../api/model/Page';
import {User} from '../../api/model/User';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['user-list.component.less']
})
export class UserListComponent implements OnInit {
  mapOfExpandData: { [key: string]: boolean } = {};

  data = [];

  constructor(private userSerive: UserService) {
  }

  ngOnInit(): void {
    this.loadData(0);
  }

  loadData(page: number) {
    this.userSerive.getAllApproverAccounts(page)
      .subscribe((data: Page<User>) => {
        this.data = data.content;
      });
  }
}
