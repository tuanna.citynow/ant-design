import {Component, OnInit} from '@angular/core';
import {UserService} from '../../api/service/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {Location} from '@angular/common';

@Component({
  selector: 'app-user-security',
  templateUrl: './user-security.component.html',
  styleUrls: ['./user-security.component.css']
})
export class UserSecurityComponent implements OnInit {

  id = -1;
  oldPassword = '';
  newPassword = '';
  confirmPassword = '';

  wrongPassword = false;
  noMatchPassword = false;

  constructor(private userService: UserService,
              private route: ActivatedRoute,
              private location: Location,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.id = +this.route.snapshot.params.id;
  }

  saveChange() {
    this.userService.changePassword(this.id, this.oldPassword, this.newPassword)
      .subscribe(v => {
        this.location.back();
      }, error => {
        this.wrongPassword = true;
      });
  }

}
