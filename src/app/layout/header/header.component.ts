import {Component, EventEmitter, HostListener, OnInit, Output} from '@angular/core';
import {NbAuthOAuth2JWTToken, NbAuthService} from '@nebular/auth';
import {Router} from '@angular/router';
import {NbLayoutRulerService} from '@nebular/theme';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

  inputSearch = '';
  user = {};
  isCollapsed = false;

  @Output('collapse')
  changeCollapse = new EventEmitter<boolean>();

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if(window.innerWidth < 992){
      this.isCollapsed = true;
      this.changeCollapse.emit(this.isCollapsed);
    }
  }


  constructor(private authService: NbAuthService,
              private router: Router) {

    this.authService.onTokenChange()
      .subscribe((token: NbAuthOAuth2JWTToken) => {
        if (token.isValid()) {
          this.user = token.getPayload()['account'];
        }
        // else {
        //   this.authService.isAuthenticatedOrRefresh().subscribe(isAuth => {
        //     console.log('Authenticated ', isAuth);
        //     if(!isAuth){
        //       this.router.navigateByUrl('/login')
        //     }
        //   });
        // }
      });
  }

  ngOnInit() {
  }

  /** custom trigger can be TemplateRef **/
  changeTrigger(): void {
    this.isCollapsed = !this.isCollapsed;
    this.changeCollapse.emit(this.isCollapsed);
  }
}
